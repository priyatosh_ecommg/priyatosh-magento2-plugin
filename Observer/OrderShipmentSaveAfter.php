<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Observer;

use SevenSenders\Shipments\Helper\Data;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order\Shipment\TrackFactory;
use Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader;
use Magento\Sales\Model\Order\Shipment;

class OrderShipmentSaveAfter implements ObserverInterface
{
    /**
     * HelperData
     * @var Data
     */
    protected $helperData;

    /**
     * Registry
     * @var Registry
     */
    protected $registry;

    /**
     * Track
     * @var TrackFactory
     */
    protected $trackFactory;

    /**
     * ShipmentLoader
     * @var \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader
     */
    protected $shipmentLoader;

    /**
     * Shipment
     * @var Shipment
     */
    protected $shipment;

    /**
     * OrderShipmentSaveAfter constructor.
     * @param Data $helperData
     * @param Registry $registry
     * @param TrackFactory $trackFactory
     * @param ShipmentLoader $shipmentLoader
     */
    public function __construct(
        Data $helperData,
        Registry $registry,
        TrackFactory $trackFactory,
        ShipmentLoader $shipmentLoader,
        Shipment $shipment
    ) {
        $this->helperData = $helperData;
        $this->registry = $registry;
        $this->trackFactory = $trackFactory;
        $this->shipmentLoader = $shipmentLoader;
        $this->shipment = $shipment;
    }

    /**
     * OrderShipmentSaveAfter
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $helper = $this->helperData;

        if (!$helper->isActive()) {
            return;
        }

        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();
        $sender = $helper->getClient();

        if (!$sender->getOptions('allowed_to_track_all_shipments')
            && $sender->getOptions('allowed_to_send_seven_senders_shipments')
            && !$shipment->getData('as_7senders')
        ) {
            return;
        }

        $order->setData('as_7senders', $shipment->getData('as_7senders'));

        if ($orderSeven = $this->helperData->createOrder($order)) {
            $this->helperData->updateShipment($shipment, null, $orderSeven);
            if (!$shipment->getData('as_7senders')) {
                $this->helperData->updateShipment($shipment, null, $orderSeven);
                return;
            }

            $shipmentSeven = $this->helperData->createShipment($shipment, $orderSeven);

            $data = [
                'carrier_code' => $this->helperData->getStoreConfig('carriers/senders7/code'),
                'title' => $this->helperData->getStoreConfig('carriers/senders7/title'),
                'number' => $shipmentSeven['reference_number'],
            ];

            $shipmentT = $this->shipment->load($shipment->getId());
            $track = $this->trackFactory->create()->addData($data);
            $this->registry->register('ignore_save', true);
            $shipmentT->addTrack($track)->save();

        }
    }
}
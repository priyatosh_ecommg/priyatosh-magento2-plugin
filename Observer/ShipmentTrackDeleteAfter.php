<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Shipment;

class ShipmentTrackDeleteAfter implements ObserverInterface
{
    /**
     * ShipmentModel
     * @var Shipment
     */
    protected $shipment;

    /**
     * ShipmentTrackDeleteAfter constructor.
     * @param Shipment $shipment
     */
    public function __construct(
        Shipment $shipment
    ) {
        $this->shipment = $shipment;
    }

    /**
     * Event Observer
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {

        $object = $observer->getEvent()->getData('data_object');
        $shipment = $this->shipment->load($object->getParentId());

        if (!$shipment->getId()) {
            return;
        }

        $shipment->setData('senders7', null);
        $shipment->save();

    }
}
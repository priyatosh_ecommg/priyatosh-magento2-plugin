<?php


namespace SevenSenders\Shipments\Helper;


class RestClient
{
    /**
     * @var $_ch
     */
    protected $_ch;

    /**
     *Token
     * @var  string $_token
     */
    protected $_token;

    /**
     * Sets token
     * @param string $token
     *
     * @return $this
     */
    public function setToken($token)
    {
        $this->_token = $token;

        return $this;
    }

    /**
     * Post action
     * @param string $json
     * @param string $url
     * @param bool $useAuth
     *
     * @return mixed
     */
    public function post($json, $url, $useAuth = true)
    {
        $this->_ch = curl_init($url);
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, true);
        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json),
        );
        if ($useAuth && $this->_token) {
            $headers[] = 'Authorization: Bearer ' . $this->_token;
        }
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($this->_ch);
        curl_close($this->_ch);

        return $result;
    }

    /**
     * Put action
     * @param string $json
     * @param string $url
     * @param bool $useAuth
     *
     * @return mixed
     */
    public function put($json, $url, $useAuth = true)
    {
        $this->_ch = curl_init($url);
        curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, true);
        $headers = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($json),
        );
        if ($useAuth && $this->_token) {
            $headers[] = 'Authorization: Bearer ' . $this->_token;
        }
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($this->_ch);
        curl_close($this->_ch);

        return $result;
    }

    /**
     * Get action
     * @param string $url
     * @param array $params
     * @param bool $useAuth
     *
     * @return mixed
     */
    public function get($url, $params = [], $useAuth = true)
    {
        if (is_array($params) && count($params)) {
            $paramsArr = [];
            foreach ($params as $key => $param) {
                if (!$param) {
                    continue;
                }
                $paramsArr[] = $key . '=' . $param;
            }
            $paramsStr = implode('&', $paramsArr);
            $url .= strlen($paramsStr) ? ('?' . $paramsStr) : '';
        }
        $this->_ch = curl_init();
        curl_setopt($this->_ch, CURLOPT_URL, $url);
        curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, true);
        if ($useAuth && $this->_token) {
            $headers[] = 'Authorization: Bearer ' . $this->_token;
        }
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($this->_ch);
        curl_close($this->_ch);

        return $result;
    }
}
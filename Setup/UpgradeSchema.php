<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $orderShipment = 'sales_shipment';
        $orderShipmentGrid = 'sales_shipment_grid';
        $fieldName = 'senders7';
        $fieldComment = 'SevenSenders';

        $setup->startSetup();

        $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderShipment),
                $fieldName,
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 2048,
                    'comment' => $fieldComment
                ]
            );

        $setup->getConnection()
            ->addColumn(
                $setup->getTable($orderShipmentGrid),
                $fieldName,
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 2048,
                    'comment' => $fieldComment
                ]
            );

        $setup->endSetup();
    }
}
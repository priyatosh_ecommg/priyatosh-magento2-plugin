<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Block\Adminhtml\Shipping\Create;

use SevenSenders\Shipments\Helper\Data as SevenSendersData;
use Magento\Backend\Block\Template\Context;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Registry;
use Magento\Sales\Helper\Data;
use Magento\Shipping\Block\Adminhtml\Create\Items as ShippingItems;
use Magento\Shipping\Model\CarrierFactory;

class Items extends ShippingItems
{

    /**
     * Helper
     *
     * @var SevenSendersData
     */
    protected $helperData;

    /**
     * Items constructor.
     * @param Context $context
     * @param StockRegistryInterface $stockRegistry
     * @param StockConfigurationInterface $stockConfiguration
     * @param Registry $registry
     * @param Data $salesData
     * @param CarrierFactory $carrierFactory
     * @param SevenSendersData $helperData
     * @param array $data
     */
    public function __construct(
        Context $context,
        StockRegistryInterface $stockRegistry,
        StockConfigurationInterface $stockConfiguration,
        Registry $registry,
        Data $salesData,
        CarrierFactory $carrierFactory,
        SevenSendersData $helperData,
        array $data = []
    ) {
        $this->helperData = $helperData;

        parent::__construct(
            $context,
            $stockRegistry,
            $stockConfiguration,
            $registry,
            $salesData,
            $carrierFactory,
            $data
        );
    }

    /**
     * Prepare child blocks
     *
     * @return string
     */
    protected function _beforeToHtml()
    {
        if ($this->helperData->showButton()) {

            $confirmText = __('You are going to create a Seven Senders shipment');

            $this->addChild(
                'submit_button_senders7',
                'Magento\Backend\Block\Widget\Button',
                [
                    'label' => __('Submit As Seven Senders Shipment'),
                    'class' => 'save submit-button secondary',
                    'onclick' => 'submitSevenSenders(this, \'' . $confirmText . '\');'
                ]
            );
        }

        return parent::_beforeToHtml();
    }
}
<?php
/**
 * Copyright © 2017 Seven Senders GmbH. All rights reserved.
 */

namespace SevenSenders\Shipments\Controller\Adminhtml\Shipment;

use SevenSenders\Shipments\Helper\Data;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Sales\Model\Order\Shipment;

class Track extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::shipment';

    /**
     * ForwardFactory
     *
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * Helper
     *
     * @var SevenSenders\Shipments\Helper\Data
     */
    protected $helperData;

    /**
     * Shipment Loader
     *
     * @var ShipmentFactory
     */
    protected $shipmentLoader;

    /**
     * Track constructor.
     * @param Context $context
     * @param ForwardFactory $resultForwardFactory
     * @param Data $helperData
     * @param Shipment $shipmentLoader
     */
    public function __construct(
        Context $context,
        ForwardFactory $resultForwardFactory,
        Data $helperData,
        Shipment $shipmentLoader
    ) {
        parent::__construct($context);
        $this->resultForwardFactory = $resultForwardFactory;
        $this->helperData = $helperData;
        $this->shipmentLoader = $shipmentLoader;
    }

    /**
     * Shipment tracking page
     *
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $resultForward = $this->resultForwardFactory->create();
        $id = $this->getRequest()->getParam('shipment_id');

        $shipment = $this->shipmentLoader->load($id);
        $url = $this->helperData->getTrackingUrl($shipment);

        if ($url) {
            $this->getResponse()->setRedirect($url);
        } else {
            return $resultForward->forward('noroute');
        }
    }
}
